require 'webrick'

server = WEBrick::HTTPServer.new :Port => 5000

sever.mount_proc '/' do |request, response|
    response.body = 'Hello World!'
end

server.start